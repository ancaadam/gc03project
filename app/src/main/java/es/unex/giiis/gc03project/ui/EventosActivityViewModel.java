package es.unex.giiis.gc03project.ui;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import es.unex.giiis.gc03project.EventosRepository;
import es.unex.giiis.gc03project.objects.Evento;

import java.util.List;

public class  EventosActivityViewModel extends ViewModel {

    private final EventosRepository mRepository;
    private final LiveData<List<Evento>> mEventos;


    //Constructor
    public EventosActivityViewModel(EventosRepository repository){
        mRepository = repository;
        mEventos = mRepository.getCurrentEventos();
    }

    //Peticion de Eventos a la BD
    public void onRefresh(){
        mRepository.getCurrentEventos();
    }

    //Devuelvo el LiveData de todos los eventos
    public LiveData<List<Evento>> getEventos() {
        return mEventos;
    }
}
