package es.unex.giiis.gc03project.network;

import android.util.Log;

import es.unex.giiis.gc03project.AppExecutors;
import es.unex.giiis.gc03project.objects.Pistas;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PistasLoaderRunnable implements Runnable{
    private static final String TAG = "PistasLoader: ";


    private final OnReposLoadedListener mOnReposLoadedListener;

    public PistasLoaderRunnable(OnReposLoadedListener onReposLoadedListener){

        mOnReposLoadedListener = onReposLoadedListener;
    }

    @Override
    public void run() {

        // Create a very simple REST adapter which points to the API.
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://opendata.caceres.es/GetData/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        OpenDataService service = retrofit.create(OpenDataService.class);//Retrofit me crea instancia de interfaz de arriba
        Call<Pistas> call = service.cogerPistas();
        try {
            Response<Pistas> response = call.execute();
            Pistas p = response.body();
            AppExecutors.getInstance().mainThread().execute(() -> mOnReposLoadedListener.onReposLoaded(p.getResults().getBindings()));
        }
        catch (IOException e){
            e.printStackTrace();
        }


    }
    private void log(String msg) {
        try {
            Thread.sleep(500);
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Log.i(TAG, msg);
    }
}



