package es.unex.giiis.gc03project.ui;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import es.unex.giiis.gc03project.EquiposRepository;

public class EquipoViewModelFactory extends ViewModelProvider.NewInstanceFactory{
    private final EquiposRepository mRepository;

    public EquipoViewModelFactory(EquiposRepository repository){
        this.mRepository = repository;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new EquiposActivityViewModel(mRepository);
    }
}
