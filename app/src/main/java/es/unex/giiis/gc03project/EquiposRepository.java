package es.unex.giiis.gc03project;

import android.util.Log;

import androidx.lifecycle.LiveData;

import es.unex.giiis.gc03project.objects.Equipo;
import es.unex.giiis.gc03project.room_db.TeamMatchDAO;

import java.util.List;

public class EquiposRepository {
    private static final String LOG_TAG = "";
    private static EquiposRepository sInstance;
    private final TeamMatchDAO mTeamMatchDao;

    public EquiposRepository(TeamMatchDAO mTeamMatchDao) {
        this.mTeamMatchDao = mTeamMatchDao;
    }

    public synchronized static EquiposRepository getInstance(TeamMatchDAO dao) {
        Log.d(LOG_TAG, "Getting the repository of equipo");
        if (sInstance == null) {
            sInstance = new EquiposRepository(dao);
            Log.d(LOG_TAG, "Made new repository of equipo");
        }
        return sInstance;
    }

    public LiveData<List<Equipo>> getCurrentEventos() {
        // Return LiveData from Room. Use Transformation to get owner
        //Ahora devolvemos una transformación.
        //Cogemos LiveData
        return mTeamMatchDao.getLiveDataAllEquipos();
    }
}
