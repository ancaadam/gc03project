package es.unex.giiis.gc03project;

import android.content.Context;

import es.unex.giiis.gc03project.network.PistasNetworkDataSource;
import es.unex.giiis.gc03project.room_db.TeamMatchDataBase;
import es.unex.giiis.gc03project.ui.PistaViewModelFactory;


/**
 * Provides static methods to inject the various classes needed for the app
 */
public class InjectorUtils {

    public static PistasRepository provideRepository(Context context) {
        TeamMatchDataBase database = TeamMatchDataBase.getInstance(context.getApplicationContext());
        PistasNetworkDataSource networkDataSource = PistasNetworkDataSource.getInstance();
        return PistasRepository.getInstance(database.getDao(), networkDataSource);
    }

    public static PistaViewModelFactory provideMainActivityViewModelFactory(Context context) {
        PistasRepository repository = provideRepository(context.getApplicationContext());
        return new PistaViewModelFactory(repository);
    }

}