package es.unex.giiis.gc03project;

import androidx.test.filters.LargeTest;
import androidx.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import androidx.test.rule.ActivityTestRule;
import es.unex.giiis.gc03project.activities.MainActivity;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.openContextualActionModeOverflowMenu;
import static androidx.test.espresso.action.ViewActions.clearText;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class RegisterUserUITest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule =
            new ActivityTestRule<>(MainActivity.class);

    @Test
    public void ShouldRegisterUser(){
        String testingUsername = "TestnameUser";
        String testingEmail = "test@test";
        String testPassword = "pruebapassword";
        String testConfPassword = "pruebapassword";

        // Perform typeText() and closeSoftKeyboard() actions on R.id.title
        onView(withId(R.id.et_username_register)).perform(typeText(testingUsername), closeSoftKeyboard());
        // Perform typeText() and closeSoftKeyboard() actions on R.id.title
        onView(withId(R.id.et_email_username)).perform(typeText(testingEmail), closeSoftKeyboard());
        // Perform typeText() and closeSoftKeyboard() actions on R.id.title
        onView(withId(R.id.et_password)).perform(typeText(testPassword), closeSoftKeyboard());
        // Perform typeText() and closeSoftKeyboard() actions on R.id.title
        onView(withId(R.id.et_repassword_register)).perform(typeText(testConfPassword), closeSoftKeyboard());

        onView(withId(R.id.btn_register)).perform(click());
    }

    @Before
    public void GoRegister(){
        openContextualActionModeOverflowMenu();
        onView(withText(R.string.action_login)).perform(click());
        onView(withId(R.id.imagebuttonRight)).perform(click());
    }
}
