package es.unex.giiis.gc03project;

import es.unex.giiis.gc03project.objects.ParticipacionUserEvento;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

public class ParticipacionUnitTest {
    public static ParticipacionUserEvento p1;

    @Before
    public void initClass(){
        p1 = new ParticipacionUserEvento(1,1);
    }

    @Test
    public void testEvento(){
        assertNotNull(p1);
    }

    @Test
    public void testUserID(){
        assertNotNull(p1.getIdUser());
        assertNotEquals(p1.getIdUser(),0);
        assertEquals(p1.getIdUser(),1);
    }

    @Test
    public void testEventoID(){
        assertNotNull(p1.getIdEvento());
        assertNotEquals(p1.getIdEvento(),0);
        assertEquals(p1.getIdEvento(),1);
    }

}
